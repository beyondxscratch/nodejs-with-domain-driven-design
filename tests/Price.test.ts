import test from 'ava';
import { Price } from '../src/domain/Price';

test('price should not be zero', async (t) => {
  const error = await t.throws(() => {
    new Price(0);
  });
  t.is(error.message, 'Price must be strictly possitive');
});

test('price should not be negative', async (t) => {
  const error = await t.throws(() => {
    new Price(-10);
  });
  t.is(error.message, 'Price must be strictly possitive');
});

test('price should be created', async (t) => {
  const price = new Price(10);
  t.is(price.amount, 10);
  t.is(price.currency, 'EUR');
});
