import express from 'express';
import { API } from '../domain/api/API';
import { Product } from '../domain/Product';
export async function startServer(port: number, api: API) {
  const app = express();

  app.get('/products', (_req, res) => {
    const { listProducts } = api;
    const products: Product[] = listProducts();
    res.send(products);
  });

  await new Promise((resolve) => app.listen(port, () => resolve(app)));
}
