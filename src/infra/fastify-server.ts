import makeFastify from 'fastify';
import { API } from '../domain/api/API';
import { Product } from '../domain/Product';

const fastify = makeFastify();

export async function startServer(port: number, api: API) {
  fastify.get('/products', async (_request, _reply) => {
    const { listProducts } = api;
    const products: Product[] = listProducts();
    return products;
  });

  await fastify.listen(port);
}
