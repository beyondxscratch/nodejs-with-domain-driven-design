import { api, API } from '../domain/api/API';
import { SPI } from '../domain/spi/Spi';
//TODO : will be the real db once the SQL client is developed
import { findAllProducts } from '../stubs/InMemoryProducts';

const makeSPI = (): SPI => ({
  findAllProducts,
});

export const makeAPI = (): API => api(makeSPI());
