import { Product } from '../Product';
import { SPI } from '../spi/Spi';
import { Store } from '../Store';

export type ListProducts = () => Product[];
export type SearchProducts = (productName: string) => Product[];

export interface API {
  listProducts: ListProducts;
  searchProducts: SearchProducts;
}

export const api = (spi: SPI): API => {
  return new Store(spi.findAllProducts);
};
