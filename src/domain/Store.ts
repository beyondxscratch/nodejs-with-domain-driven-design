import { API, ListProducts, SearchProducts } from './api/API';
import { Product } from './Product';
import { FindAllProducts } from './spi/Spi';

export class Store implements API {
  constructor(private findAllProducts: FindAllProducts) {}

  searchProducts: SearchProducts = (productName: string) => {
    const products = this.findAllProducts();
    return products.filter((product: Product) => product.name === productName);
  };

  listProducts: ListProducts = () => {
    return this.findAllProducts();
  };
}
