export class Price {
  constructor(
    public readonly amount: number,
    public readonly currency: 'EUR' = 'EUR'
  ) {
    if (amount <= 0) {
      throw new Error('Price must be strictly possitive');
    }
  }
}
